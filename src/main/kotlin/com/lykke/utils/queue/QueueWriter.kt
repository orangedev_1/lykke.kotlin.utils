package com.lykke.utils.queue

internal interface QueueWriter {
    fun write(data: String)
}